compass-h5bp-plugin (1.0.0-6) unstable; urgency=medium

  * relax to build-depend unversioned on ruby-sass:
    needed version satisfied in all supported releases of Debian
  * stop depend on ruby ruby-sass
  * provide virtual package sass-stylesheets-h5bp
  * use debhelper compatibility level 13 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * extend patch 2002 to fix upstream reproducibility bug;
    closes: bug#862088, thanks to Chris Lamb
  * declare compliance with Debian Policy 4.5.1
  * update source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Mar 2021 13:29:14 +0100

compass-h5bp-plugin (1.0.0-5) unstable; urgency=medium

  * Simplify rules.
    Stop build-depend on licensecheck cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Update Vcs-* URLs: Maintenance moved to Salsa.
  * Set Rules-Requires-Root: no.
  * Talk about Sass (not Compass) in short and long description.
  * Update copyright info:
    + Use https protocol in Format and Upstream-Contact URLs.
    + Drop superfluous alternate Source URL.
    + Extend coverage of packaging.
  * Update watch file:
    + Simplify regular expressions.
    + Rewrite usage comment.
  * Tighten lintian overrides regarding License-Reference.
  * Advertise DEP3 format in patch headers.
  * Include sass files below /usr/share/sass.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Mar 2019 19:41:14 +0100

compass-h5bp-plugin (1.0.0-4) unstable; urgency=medium

  * Fix patch 2001 to work with compass.
    Closes: Bug#854308.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Mar 2017 19:55:37 +0100

compass-h5bp-plugin (1.0.0-3) unstable; urgency=medium

  * Tidy patch 2001 and fix require Sass (not Compass).
    Unfuzz patch 2002.
  * Bump debhelper compatibility level to 9.
  * Update watch file:
    + Bump to version 4.
    + Add usage comment.
    + Use github pattern from documentation.
  * Update git-buildpage config:
    + Avoid git- prefix.
    + Filter any .git* file.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of Debian packaging.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Use git subdir (not cgit).
    + Add .git suffix for Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Update package relations:
    + Relax dependency on ruby-sass.
    + Stop fallback-depend on ruby-compass.
    + Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Jan 2017 11:22:31 +0100

compass-h5bp-plugin (1.0.0-2) unstable; urgency=medium

  * Update copyright info: Extend coverage for myself.
  * Modernize git-buildpackage config: Drop "git-" prefix.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 16 May 2015 04:28:39 +0200

compass-h5bp-plugin (1.0.0-1) experimental; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Unfuzz (and slightly improve) patches.
  * Add NEWS file waring about major changes to mixins.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 05 Mar 2015 13:52:37 +0100

compass-h5bp-plugin (0.1.2-4) unstable; urgency=medium

  * Fix update patch 2001 to properly register path.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Oct 2014 07:29:02 +0200

compass-h5bp-plugin (0.1.2-3) unstable; urgency=medium

  * Update Vcs-Browser URL to use cgit web frontend.
  * Fix use canonical URL in Vcs-Git.
  * Declare compliance with Debian Policy 3.9.6.
  * Add patch 2001 to register system-shared framework install path.
  * Build and install using gem2deb.
  * Build-depend on gem2deb.
  * Depend on ruby.
  * Build-depend on recent ruby-sass, favored over ruby-compass.
  * Git-ignore quilt .pc dir.
  * Add patch 2002 to avoid git in gemspec.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Oct 2014 05:22:12 +0200

compass-h5bp-plugin (0.1.2-2) unstable; urgency=medium

  * Update Maintainer, Uploaders and Vcs-* fields: Packaging git moved
    to pkg-sass area of Alioth.
  * Fix use package section web.
  * Extend coverage for myself, and relicense packaging as GPL-3+.
  * Bump standards-version to 3.9.5.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 May 2014 17:13:41 +0200

compass-h5bp-plugin (0.1.2-1) unstable; urgency=low

  [ upstream ]
  * New release.
    (no code changes since 0.1.1+20130730 snapshot)

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 30 Oct 2013 13:17:19 +0100

compass-h5bp-plugin (0.1.1+20130730-1) unstable; urgency=low

  [ upstream ]
  * Snapshot from git.
    + Test more common use for print styles
    + Update normalize.css to v1.1.1
    + Fix _h5bp.scss to not cause trouble in the sass compiler.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to CDBS+git-buildpackage
    wiki page for details.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 20 Oct 2013 01:53:57 +0200

compass-h5bp-plugin (0.1.1-1) unstable; urgency=low

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Have git-buildpackage ignore upstream .gitignore files.
  * Stop track md5sum of upstream tarball.
  * Bump standards-version to 3.9.4.
  * Use anonscm.debian.org URL for Vcs-Git.
  * Update short and long description:
    + Fix capitalize HTML5.
    + Stop mentioning authors.
    + Mention H5BP abbreviation
    + Mention Normalize.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 08 Apr 2013 12:01:16 +0200

compass-h5bp-plugin (0.1.0-1) unstable; urgency=low

  [ upstream ]
  * New upstream release.

  [ Jonas Smedegaard ]
  * Update watch and rules files to directly use github.com URL (not
    githubredir.debian.net).
  * Bump dephelper compatibility level to 8.
  * Use anonscm.debian.org for Vcs-Browser field.
  * Build-depend on devscripts, to always check copyrights during build.
  * Update copyright file:
    + Bump to copyright format 1.0.
    + Fix use pseudo-comment section to obey silly restrictions of
      copyright format 1.0.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 03 Dec 2012 19:43:03 +0100

compass-h5bp-plugin (0.0.5-1) unstable; urgency=low

  * Initial release.
    Closes: #661972.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Mar 2012 03:16:21 +0100
